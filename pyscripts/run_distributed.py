# !python
#
# Run framework app distributed.
#
import os
import shutil
import pathlib

import parsl
from parsl.app.app import python_app, bash_app
from parsl.configs.local_threads import config
from parsl.config import Config

# Count the nubmer of times we've looked at a run number
run_numbers_used = {}

@python_app
def quick_run(inputs=[], outputs=[]):
    r"""
    Return dummy value since we just need a future so everyone knows what
    happened (and data futures)
    """
    return 0

def start_run(cmd, run_number, base_folder, output_files, prefix_commands):
    r"""
    Runs a bash command (returns a future) and puts the output files
    in the proper custom directory.
    """
    # Since this will produce a special folder, we need to create a directory where it will run.
    # If the folder already exists, remove it. Make sure we can run on the same run multiple times.
    # This is a global variable. Sorry!
    global run_numbers_used
    if run_number in run_numbers_used:
        run_numbers_used[run_number] += 1
    else:
        run_numbers_used[run_number] = 0
    folder = "%s/run_%d_%d" % (base_folder, run_number, run_numbers_used[run_number])

    # Now, properly build the list of output files.
    outputs = ["%s/%s" % (folder, of) for of in output_files]

    # Check to see if the run has already completed. If so, then return a simple dummy future.
    if os.path.isdir(folder) and all((os.path.isfile(fn) for fn in outputs)):
        return quick_run(outputs=outputs)

    # If the folder does not exist, create it.
    if (os.path.isdir(folder)):
        shutil.rmtree(folder)
    os.mkdir(folder)
    
    # And run it
    pCmd = "" if len(prefix_commands) == 0 else " && ".join(prefix_commands) + " && "
    return start_run_bash("%s%s %d" % (pCmd, cmd, run_number), folder, outputs=outputs, stdout="%s/stdout.log" % folder, stderr="%s/stderr.log" % folder)

@bash_app
def start_run_bash(cmd, folder, outputs=[], stdout='stdout.log', stderr='stderr.log'):
    r"""
    Run the command with the output file as expected.
    """

    # Run there.
    return "cd %s; %s" % (folder, cmd)

@bash_app
def copy_file_bash(inputs=[], outputs=[]):
    r"""
    Copy a file from one location to the other
    """
    return "cp %s %s" % (inputs[0], outputs[0])

def copy_file(source, output_location):
    r"""
    Copya single file to output
    """
    output = "%s/%s" % (output_location, os.path.basename(source.filepath))
    copied_future = copy_file_bash(inputs=[source], outputs=[output])
    return copied_future.outputs[0]

@bash_app
def add_file_bash(inputs=[], outputs=[], cmd_prefix="", stdout='add-file-bash.log', stderr='add-file-bash.err'):
    r"""
    Use hadd to add together two root files
    """
    import os
    bad_files = [i.filepath for i in inputs if not os.path.isfile(i.filepath)]
    if len(bad_files) > 0:
        print ("Internal error - missing input files: %s" % bad_files)
        print (inputs)
        raise Exception("Internal error - missing input files %s" % bad_files)

    pCmd = "" if len(cmd_prefix) == 0 else " && ".join(cmd_prefix) + " && "
    
    return "%s hadd -f %s %s" % (pCmd, outputs[0], " ".join([i.filepath for i in inputs]))

@python_app
def add_file_collate_python(inputs=[], outputs=[]):
    r"""
    Null app that just takes its inputs and turns them into outputs.
    This is just a way to build an output routine.
    """
    return 0

def add_file(input_list, output_location, cmd_prefix):
    r"""
    Combine the input files. The output will be same name.

    input_list: A list of job futures. Each one has the same set of output futures attached to it.
    output_location: where we should copy the output files to.
    """
    # Create the output file location.
    if not os.path.isdir(output_location):
        os.mkdir(output_location)

    # Next, we need to add the list of output files.
    output_filename = "%s/%s" % (output_location, os.path.basename(input_list[0].filepath))
    r = add_file_bash(cmd_prefix=cmd_prefix, 
        inputs=[i for i in input_list],
        outputs=[output_filename],
        stdout="%s/stdout.log" % output_location,
        stderr="%s/stderr.log" % output_location)
    
    return r.outputs[0]

def split(a, n):
    k, m = divmod(len(a), n)
    total_split = k if m == 0 else k+1
    return (a[i * n:min((i + 1) * n, len(a))] for i in range(total_split))

def combine_files(file_future_list, output_location, cmd_prefix, depth = 0):
    r"""
    GIven the list of data futures, as they are done, run the add to combine
    them.

    file_future_list: A list of all futures we want to combine outputs. We assume
                      the exact same job is used here!
    output_location: where we want the files to end up.
    cmd_prefox: Code to run before we span anything
    dept: So we can see ho far down the hole we've gotten.

    return: a list of file data futures
    """
    # special case if future list is one
    if len(file_future_list)==1:
        return copy_file(file_future_list[0], output_location)

    # Do we have so many inputs that we need to split them and do the combination
    # in a hierarchical way/structure?
    future_list_split = list(split(file_future_list, 20))
    if len(future_list_split) > 1:
        all_combine_jobs = [combine_files(fl, "%s/combine_i%d_d%d" % (output_location, index, depth), cmd_prefix) for index,fl in enumerate(future_list_split)]
        return combine_files(all_combine_jobs, output_location, cmd_prefix, depth+1)
    
    # Small enough that we can return a combination of one here.

    return add_file(file_future_list, output_location, cmd_prefix)

def combine_job_files(job_list, output_location, cmd_prefix):
    r"""
    Given a list of jobs with identical outputs, combine the outputs.

    job_list: List of the app futures for all the jobs to combine
    output_location: Where we want to stach the final data files.
    cmd_prefix: What to execute before we run anything. Env setup, etc.
    
    returns: List of DataFutures, one for each output in the job.
    """

    # Build a list of files
    files = [[f.outputs[i] for f in job_list] for i in range(len(job_list[0].outputs))]

    # Now, combine each one
    return [combine_files(flist, output_location, cmd_prefix) for flist in files]

def run_command (cmd, run_list, base_folder, output_files, cmd_prefix):
    r"""
    Run a command in a distrubted fashion. All the available guys are run.

    cmd: String of the command that we are to run
    run_list: array of run numbers that we are to tack onto the end of the command for each run
    base_folder: an already created folder where we can put outputs for this app
    output_files: list of files that the app produces when it runs that we are to collate
    cmd_prefix: Commands that should be executed before each attempt to run

    returns: the data file future for the output
    """
    # can we figure out where this command is?
    cmd = os.path.abspath(cmd)
    if not os.path.isfile(cmd):
        raise Exception("Unable to find the location of the program you want to run")

    # Make sure the output directory exists. Don't blow it away if it doesn't.
    a_base_folder = os.path.abspath(base_folder)
    if not os.path.isdir(a_base_folder):
        os.mkdir(a_base_folder)

    # Start everything up with the futures to run!
    job_run_list = [start_run(cmd, r, a_base_folder, output_files, cmd_prefix) for r in run_list]

    # next, combine them in to a list of files.
    return combine_job_files(job_run_list, a_base_folder, cmd_prefix)

@bash_app
def run_bash_command(cmd, stdout='output.txt', stderr='stderr.txt'):
    # Remove fiels if they already exist.
    import os
    if os.path.isfile(stdout):
        os.unlink(stdout)
    if os.path.isfile(stderr):
        os.unlink(stderr)
    return cmd

def get_run_list(runinfo, prefix_commands, dir):
    r"""
    Given a standard command line, convert it to a list of runs we can use to feed the
    runner. Use the frameworks' code to do this. We are expecting built images to be in a certain
    location for this to work!

    runinfo: run to write everything out
    prefix_commands: setup commands to issue first to setup the enviornment
    dir: directory where output files should be written.
    """
    script_dir = os.path.dirname(os.path.abspath(__file__))
    exe_dir = "%s/install/bin/DumpRunList" % script_dir
    if not os.path.isfile(exe_dir):
        raise Exception("Unable to find DumpRunList at {}".format(exe_dir))

    pCmd = "" if len(prefix_commands) == 0 else " && ".join(prefix_commands)
    output_dir = dir + "/run_list"
    pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True) 

    run_bash_command("%s && cd %s && echo NOWAYDUDE && %s %s" % (pCmd, output_dir, exe_dir, runinfo), stdout= output_dir + '/get_run_list.log').result()
    with open(output_dir + '/get_run_list.log') as f:
        lines = f.readlines()
        for index, l in enumerate(lines):
            if "NOWAYDUDE" == l.strip():
                first_good_item = index
                break
        lines = lines[first_good_item+1:]
        return [int(s.strip()) for s in lines]

from parsl.executors.threads import ThreadPoolExecutor
from parsl.providers import LocalProvider
from parsl.channels import SSHChannel
from multiprocessing import cpu_count
from parsl.addresses import address_by_hostname
from parsl.executors import HighThroughputExecutor
from parsl.executors.ipp import IPyParallelExecutor

# If we are driving this, then we had better get everything going!
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Run on lots of runs and combine results')
    parser.add_argument('exe_path', help='Absolute path to the executable that you wish to run along with any initial command line arguments')
    parser.add_argument('run_list', help='Standard framework arguments with the runs to process (grl, etc.)')
    parser.add_argument('dir', help='Directory where results should be stored')
    parser.add_argument('output_file', help='Output file written by the executable. Can only be a .root file for now')
    parser.add_argument('--cmd_prefix', help='Add this command as a prefix to all executed commands. Can specify more than one', action='append', default=[])
    parser.add_argument('--remote', help='Run on this remote node. Add one for every node to add to pool', action='append', default=[])
    parser.add_argument('--worker_init', help='Commands to send to the worker to get it setup before the remote execution engine is run. Add as many as you like. Executed in order.', action='append', default=[])

    args = parser.parse_args()

    # Init parsl, and load our running config
    if len(args.remote) == 0:
        # Config for running on this one local machine. Be aggressive.
        local_config = Config(
            executors=[
                ThreadPoolExecutor(
                    max_threads=cpu_count(),
                    label='local_threads'
                )
            ]
        )
        parsl.load(local_config)
    else:
        remote_config = Config(
            executors=[
#                HighThroughputExecutor(
                IPyParallelExecutor(
                    workers_per_node=cpu_count(), # For Debugging
                    label='remote_ipp_{}'.format(m),
                    #cores_per_worker=cpu_count(),
                    #address=address_by_hostname(),
                    provider=LocalProvider(
                        min_blocks=1,
                        init_blocks=1,
                        max_blocks=1,
                        nodes_per_block=1,
                        #parallelism=0.5,
                        channel=SSHChannel(hostname=m),
                        #worker_init='source /phys/groups/tev/scratch3/users/gwatts/anaconda3/etc/profile.d/conda.sh && conda activate parsl',
                        #worker_init='source /phys/groups/tev/scratch3/users/gwatts/anaconda3/etc/profile.d/conda.sh && conda activate parsl',
                        #worker_init='source /home/gwatts/anaconda3/etc/profile.d/conda.sh && export PYTHONPATH=$PYTHONPATH:{} && conda activate parsl_test'.format(os.getcwd()),
                        worker_init=" && ".join(args.worker_init),
                        move_files=None if m != "localhost" else False,
                    )
                ) for m in args.remote
            ],
            strategy=None,
        )
        parsl.load(remote_config)


    # Next, parse the run list
    abs_working_directory = os.path.abspath(args.dir)
    run_list = get_run_list(args.run_list, args.cmd_prefix, abs_working_directory)

    # Do the work!
    r = run_command (args.exe_path, run_list, abs_working_directory, [args.output_file], args.cmd_prefix)
    for j in r:
        j.result()
        print (j.filepath)
