# DistributedRunner

Simple runner for a standard MATHUSLA framework app that will run in multiple places

# Usage

This uses the distributed control language called [parsl](http://parsl-project.org/), so it will have to be installed first.
See [installation instructions](https://parsl.readthedocs.io/en/stable/quickstart.html).

There are a few requirements on usage:

- You must use an app that takes a run number on the command line. The run number will be appended to the end of the command line you supply.
- The app must output a filename that is the same. The file can only by a ROOT file, and it must be possible to combine the ROOT file with hadd.

Usage:

    usage: run_distributed.py [-h] exe_path run_list dir output_file

    Run on lots of runs and combine results

    positional arguments:
    exe_path     Absolute path to the executable that you wish to run along with
                any initial command line arguments
    run_list     Standard framework arguments with the runs to process (grl,
                etc.)
    dir          Directory where results should be stored
    output_file  Output file written by the executable. Can only be a .root file
                for now

    optional arguments:
    -h, --help   show this help message and exit

# Notes

If you re-run, it will re-use past results. Remove the directory or a particular run's sub directory if you wish to
re-run everything or a subset.

stdout and stderr are written to log files in each directory under the running directory

This script needs python 3.4 or better to run. Default configurations with ROOT for ATLAS are still using 2.7. This sets up a
huge battle. The following works well for me:

    python ../../DistributedRunner/build/run_distributed.py /phys/groups/tev/scratch3/users/gwatts/MATHUSLA/TestStandOffline/build/test/tracks 4500-4501 junkdir tracks.root --cmd_prefix "source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" --cmd_prefix "lsetup \"root recommended\""

# Outstanding Things To Do

- If a run fails, just note it, but don't fail everything. Delete the subdirectory if it fails.
- Run on multiple machines with this ipython crap. Looks like this is not provided by the package, so this might be tough for now.
- Get it working on a single run (fails b.c. soemthing isn't a list)

```
(parsl) -bash-4.2$ time python ../../DistributedRunner/build/run_distributed.py test/tracks 4500 junkdir tracks.root --cmd_prefix "source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" --cmd_prefix "lsetup \"root recommended\"" --cmd_prefix "export TESTSTANDCONFIG=/phys/groups/tev/scratch3/users/gwatts/MATHUSLA/TestStandOffline/config" --cmd_prefix 'export LD_LIBRARY_PATH=/phys/groups/tev/scratch3/users/gwatts/MATHUSLA/TestStandOffline/build:$LD_LIBRARY_PATH' --worker_init 'source /phys/groups/tev/scratch3/users/gwatts/anaconda3/etc/profile.d/conda.sh' --worker_init 'conda activate parsl'  --remote tev02.phys.washington.edu
```